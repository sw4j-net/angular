ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/nodejs20:latest

RUN <<EOF
useradd -ms /bin/bash angular
apt-get update
apt-get -y upgrade
npm install -g @angular/cli
EOF

WORKDIR /home/angular
USER angular
